from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.main, name='main'),

    url(r'^main/$', views.main, name='main2'),

    url(r'^pred/$',views.prediction),

    url(r'^index/$',views.index,name='index'),

    url(r'^main/serve/$', views.main, name='mainpred'),

    url(r'^about/$', views.about, name='about'),

    url(r'^gallery/$', views.flowchart, name='gallery'),

    url(r'^contact/$', views.contact, name='contact'),

    url(r'^first_inn/$', views.first_inn, name='first_inn'),

    # url(r'^second_inn/$', views.second_inn, name='secustom_processcond_inn'),
    url(r'^pre_pred/$', views.prematch, name='prematch'),

    url(r'^customise',views.customise,name='customise'),

    url(r'^process/$',views.customise_process,name='custom_process'),

    url(r'^getdata/$',views.get_data,name='get'),

    url(r'^getteam1/$',views.get_team1,name='team1'),

    url(r'^season_customise/$',views.season_customise,name='season_customise'),

    url(r'^season_process/$', views.season_customise_process, name='season_customise_process'),

    url(r'^fetch_teams/$',views.fetch_teams,name='fetch_teams'),

    url(r'^show/$',views.show_template,name=''),
]
