from django.shortcuts import render
from django.http import JsonResponse
from .prediction import predict
import requests
import pandas as pd
import csv
from _pydecimal import Decimal
import operator
from numpy import unicode

winner_list = []
Eigen_score = []
result = []
season_result = []
test = []
players =[]
Eigen_1 = []
Eigen_2 = []
Eigen_3 = []
Eigen_list = []
custom_winner_list = []

# checking
from django.shortcuts import render, render_to_response

from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter

from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import components


def show_template(request):
    return render(request, 'fourth_umpire/player_swap.html')


def season_customise(request):
    print("year ", request.POST['year24'])
    season = request.POST['year24']

    context = {
        'season': season,
    }

    return render(request, 'fourth_umpire/player_swap.html', context)


def season_customise_process(request):

    print("post data ",request.POST)

def fetch_teams(request):
    print("season ", request.GET.get('season'))
    f = open('fourth_umpire/Dataset/results.csv')
    csv_f = csv.reader(f)
    next(csv_f)
    team_list = [];
    other_teams = {}
    for row in csv_f:
        team = row[0]
        team_list.append(team)
        players = list(row[1].split(','))
        for x in range(0, len(players)):
            other_teams[team] = list(row[1].split(','))

    data = {
        'other_teams': other_teams,
        'team':team_list,
    }

    return JsonResponse(data)


def index(request):
    # df = pd.read_csv('fourth_umpire/Dataset/_team_rank.csv')
    # print(df)
    # for row in df:
    #     print(row)
    # GUJARAT
    x = []
    y = []
    a = []
    with open('fourth_umpire/Dataset/_team_rank.csv', 'r') as csvfile:
        next(csvfile)
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            x.append(row[0])
            y.append(row[1])

    for team in x:
        if team == "Chennai Super Kings":
            a.append('CSK')

        if team == "Mumbai Indians":
            a.append('MI')

        if team == "Rajasthan Royals":
            a.append('RR')

        if team == "Kolkata Knight Riders":
            a.append('KKR')

        if team == "Delhi Daredevils":
            a.append('DD')

        if team == "Royal Challengers Bangalore":
            a.append('RCB')

        if team == "Kings XI Punjab":
            a.append('KXIP')

        if team == "Deccan Chargers":
            a.append('DC')

        if team == "Pune Warriors":
            a.append('PW')

    print("teams ", a)
    print("eigens ", y)

    # x = ['MI', 'CSK', 'KKR', 'RCB', 'SRH', 'DD', 'KXIP', 'RR']
    p = figure(x_range=a, plot_height=250, title="Team rank based on eigen score")
    p.vbar(x=a, top=y, width=0.9)

    p.xgrid.grid_line_color = None
    p.y_range.start = 0
    script, div = components(p, CDN)

    #
    # print(script)
    # print(div)
    # show(p)
    # output_file('fourth_umpire/templates/fourth_umpire/c.html')

    return render_to_response('fourth_umpire/c.html',
                              {'script': script, 'div': div, }
                              )


# checking


def main(request):
    return render(request, 'fourth_umpire/main.html')


def about(request):
    return render(request, 'fourth_umpire/about.html')


def contact(request):
    return render(request, 'fourth_umpire/contact.html')


def flowchart(request):
    return render(request, 'fourth_umpire/gallery.html')


def test(request):
    return render(request, 'fourth_umpire/remove_player.html')


def prediction(season):
    url = 'http://www.cricmetric.com/ipl/ranks/' + season
    pd.read_html(requests.get(url).content)[-1].to_csv("fourth_umpire/Dataset/_player_rank.csv", index=False)
    url = 'http://www.cricmetric.com/series.py?series=ipl' + season + '&show=matches'
    pd.read_html(requests.get(url).content)[0].to_csv("fourth_umpire/Dataset/match_list.csv", index=False)
    # url = 'https://www.sportskeeda.com/go/ipl/points-table'
    # pd.read_html(requests.get(url).content)[0].to_csv("test.csv", index=False)
    data = pd.read_csv("fourth_umpire/Dataset/_player_rank.csv")
    team_rank = pd.DataFrame(
        data.groupby('Team')['EFscore'].agg(['sum']).reset_index().sort_values('sum', ascending=False)).to_csv(
        "fourth_umpire/Dataset/_team_rank.csv", index=False)
    df = pd.read_csv('fourth_umpire/Dataset/_team_rank.csv')
    # fig = px.bar(df, x='Team', y='sum', title='Team Rankings based on eigen score For Season -' + season)
    # fig.write_image("./images/fig1.png")
    # fig.show()

    # these 3 lines have to work rankings csv file not found

    # data = pd.read_csv('fourth_umpire/Dataset/rankings.csv')
    # rankings = data.groupby(['Team']).sum()
    # export_csv = rankings.to_csv(r'fourth_umpire/Dataset/results.csv')

    # response = HttpResponse(mimetype="./images/fig1.png")
    # fig.save(response, "PNG")
    with open('fourth_umpire/Dataset/_team_rank.csv', 'rt')as f:
        data = csv.reader(f)
        for row in data:
            winner_list.append(row[0])
            Eigen_score.append(row[1])
    print("The Predicted winner Team " + "for the season " + season + " is ---------- " + winner_list[
        1] + " ------------ With Eigen Score Of " + Eigen_score[1])

    f = open('fourth_umpire/Dataset/results.csv')
    csv_f = csv.reader(f)
    for row in csv_f:
        team = row[0]
        players = list(row[1].split(','))
        print("ss ", len(players))
        print("t ", team)
        for x in range(0, len(players)):
            # print("x ", x)
            #             # test.update({players[x]: team})
            with open('fourth_umpire/Dataset/create.csv', 'a') as fd:
                fd.write(team)
                fd.write(',')
                fd.write(players[x])
                fd.write('\n')

def customise(request):
    print("year ", request.POST['year23'])
    season = request.POST['year23']
    prediction(season)
    f = open('fourth_umpire/Dataset/match_list.csv')
    csv_f = csv.reader(f)
    next(csv_f)

    context = {
        'csv': csv_f,
        'season': season,
    }
    return render(request, 'fourth_umpire/customize.html', context)


def customise_process(request):
    # print(request.POST)
    player1 = request.POST.getlist('player1')
    player2 = request.POST.getlist('player2')
    match_no = request.POST['Match_No']
    f = open('fourth_umpire/Dataset/match_list.csv')
    csv_f = csv.reader(f)
    for row in csv_f:
        if (row[0] == match_no):
            actual_winner = row[5]
    venue = request.POST['venue']
    toss_win = request.POST['toss_win']
    toss_decision = request.POST['toss']
    team1 = request.POST['team1']
    team2 = request.POST['team2']
    weather = request.POST['weather']
    season = request.POST['year']
    print("match_no", match_no)
    print("venue", venue)
    print("toss_win", toss_win)
    print("toss_desicion", toss_decision)
    print("weather", weather)
    print("season ", season)
    f = open('fourth_umpire/Dataset/_player_rank.csv')
    csv_f = csv.reader(f)
    for row in csv_f:
        for i in range(len(player1)):
            if (row[1] == player1[i]):
                Eigen_1.append(Decimal(row[5]))
            if (row[1] == player2[i]):
                Eigen_2.append(Decimal(row[5]))
    t1 = sum(Eigen_1)
    t2 = sum(Eigen_2)
    # for home ground privilages
    if ((team1 == 'Chennai Super Kings') and (venue == '8')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Deccan Chargers') and (venue == '1')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Delhi Daredevils') and (venue == '7')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Kings XI Punjab') and (venue == '13')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Kolkata Knight Riders') and (venue == '12')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Mumbai Indians') and (venue == '6')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Rajasthan Royals') and (venue == '15')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team1 == 'Royal Challengers Bangalore') and (venue == '5')):
        t1 = float(t1) + 0.25 * float(t1)
    if ((team2 == 'Chennai Super Kings') and (venue == '8')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Deccan Chargers') and (venue == '1')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Delhi Daredevils') and (venue == '7')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Kings XI Punjab') and (venue == '13')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Kolkata Knight Riders') and (venue == '12')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Mumbai Indians') and (venue == '6')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Rajasthan Royals') and (venue == '15')):
        t2 = float(t2) + 0.25 * float(t2)
    if ((team2 == 'Royal Challengers Bangalore') and (venue == '5')):
        t2 = float(t2) + 0.25 * float(t2)
    # toss win and weather conditions
    if (toss_win == '1'):
        if (toss_decision == '1'):
            if ((weather == '1') or (weather == '2')):
                t1 = float(t1) + 0.25 * float(t1)
            else:
                t1 = float(t1) - 0.25 * float(t1)
        else:
            if ((weather == '3') or (weather == '4') or (weather == '5')):
                t1 = float(t1) + 0.25 * float(t1)
            else:
                t1 = float(t1) - 0.25 * float(t1)
    else:
        if (toss_decision == '1'):
            if ((weather == '1') or (weather == '2')):
                t2 = float(t2) + 0.25 * float(t2)
            else:
                t2 = float(t2) - 0.25 * float(t2)
        else:
            if ((weather == '3') or (weather == '4') or (weather == '5')):
                t2 = float(t2) + 0.25 * float(t2)
            else:
                t2 = float(t2) - 0.25 * float(t2)

    f = open('fourth_umpire/Dataset/results.csv')
    csv_f = csv.reader(f)
    other_teams = {}
    for row in csv_f:
        team = row[0]
        players = list(row[1].split(','))
        for x in range(0, len(players)):
            other_teams[team] = list(row[1].split(','))

    # print("old ", other_teams)

    if team1 in other_teams:
        del other_teams[team1]
    if team2 in other_teams:
        del other_teams[team2]
    # print("new ", other_teams)
    f = open('fourth_umpire/Dataset/custom_team_rank.csv', 'w')
    f.truncate()
    f.close()
    with open('fourth_umpire/Dataset/custom_team_rank.csv', 'a') as fd:
        fd.write('Team')
        fd.write(',')
        fd.write('sum')
        fd.write('\n')
        fd.close()
    for key in other_teams:
        team_players = (other_teams[key])
        if (key != 'Team'):
            f = open('fourth_umpire/Dataset/_player_rank.csv')
            csv_f = csv.reader(f)
            for row in csv_f:
                for i in range(len(team_players)):
                    if (row[1] == team_players[i]):
                        Eigen_3.append(Decimal(row[5]))
            Eigen_list.append(sum(Eigen_3))
            with open('fourth_umpire/Dataset/custom_team_rank.csv', 'a') as fd:
                fd.write(key)
                fd.write(',')
                fd.write(str(sum(Eigen_3)))
                fd.write('\n')
            Eigen_3.clear()
    with open('fourth_umpire/Dataset/custom_team_rank.csv', 'a') as fd:
        fd.write(str(team1))
        fd.write(',')
        fd.write(str(t1))
        fd.write('\n')
        fd.write(str(team2))
        fd.write(',')
        fd.write(str(t2))
        fd.write('\n')
        t1 = str(t1)
        t2 = str(t2)
    if (t1 > t2):
        winner = team1
    else:
        winner = team2
    print("-----winner by eigen values after customisation is -----")
    print(winner)
    # to find the customised season winner
    f = open('fourth_umpire/Dataset/custom_team_rank.csv')
    csv_f = csv.reader(f)
    for row in csv_f:
        if (row[1] != 'sum'):
            custom_winner_list.append(row[1])
    Max_eigen = max(custom_winner_list)
    # for finding the winner of customised season
    f = open('fourth_umpire/Dataset/custom_team_rank.csv')
    csv_f = csv.reader(f)
    for row in csv_f:
        if (row[1] == Max_eigen):
            season_winner = row[0]
    print("season winner can be ", season_winner)

    context = {'team1': t1,
               'team2': t2, 'match_no': match_no, 'winner': winner,
               'season': season, 'actual_winner': actual_winner,
               }
    print(context)

    return render(request, 'fourth_umpire/pre_pred_customize.html', context)

    # post data
    # <QueryDict: {'Match_No': ['24'], 'player1': ['Parthiv Patel', 'Washington Sundar', 'Moeen Ali', 'Colin de Grandhomme'],
    # 'player2': ['Faf du Plessis', 'M Vijay', 'Shane Watson', 'Ravindra Jadeja'],
    # 'venue': ['2'], 'toss_win': ['2'], 'toss': ['2']}>

    # to get data from request
    match_no = request.POST['Match_No']
    # print("customise_process")


# def customise_process(request):
#     #print(request.POST)
#
#     player1 = request.POST.getlist('player1')
#     player2 = request.POST.getlist('player2')
#
#     print("p1 ",player1)
#     print("p2 ",player2)
#     # 'player1': ['S Dhawan', 'CL White', 'JP Duminy', 'DW Steyn', 'DJ Harris']
#     # post data
#     # <QueryDict: {'Match_No': ['24'], 'player1': ['Parthiv Patel', 'Washington Sundar', 'Moeen Ali', 'Colin de Grandhomme'],
#     # 'player2': ['Faf du Plessis', 'M Vijay', 'Shane Watson', 'Ravindra Jadeja'],
#     # 'venue': ['2'], 'toss_win': ['2'], 'toss': ['2']}>
#
#     # to get data from request
#     match_no = request.POST['Match_No']
#     team1 = request.POST['team1']
#     team2 = request.POST['team2']
#     print(match_no)
#
#     f = open('fourth_umpire/Dataset/results.csv')
#     csv_f = csv.reader(f)
#
#     other_teams = {}
#     for row in csv_f:
#         team = row[0]
#         players = list(row[1].split(','))
#         for x in range(0, len(players)):
#             other_teams[team] = list(row[1].split(','))
#
#     print("old ", other_teams)
#
#     if team1 in other_teams:
#         del other_teams[team1]
#     if team2 in other_teams:
#         del other_teams[team2]
#     print("new ", other_teams)


def get_team1(request):
    team1 = request.GET.get('team1')
    print("team1 ", team1)
    f = open('fourth_umpire/Dataset/match_list.csv')
    csv_f = csv.reader(f)
    headers = next(csv_f)
    count = 0;

    for row in csv_f:
        print("c ", count)
        if team_limit == 15:
            break
        else:
            if row[0] == team1:
                player1 = row[1]
                team_limit = team_limit + 1

    data = {
        'player1': player1,
    }
    return JsonResponse(data)


def get_data(request):
    limit = 15
    match_no = request.GET.get('Match_No', 'empty')
    print("match no ", match_no)
    f = open('fourth_umpire/Dataset/match_list.csv')
    csv_f = csv.reader(f)
    headers = next(csv_f)
    player1 = []
    player2 = []
    count = 0

    for row in csv_f:
        if row[0] == match_no:
            team1 = row[1]
            team2 = row[2]
            print("teams for given match ", team1, team2)

    # match_f = open('fourth_umpire/Dataset/players_list.csv')
    f = open('fourth_umpire/Dataset/results.csv')
    csv_f = csv.reader(f)

    other_teams = {}
    for row in csv_f:
        team = row[0]
        players = list(row[1].split(','))
        for x in range(0, len(players)):
            other_teams[team] = list(row[1].split(','))

    print("old ", other_teams)

    if team1 in other_teams:
        del other_teams[team1]
    if team2 in other_teams:
        del other_teams[team2]
    print("new ", other_teams)

    match_f = open('fourth_umpire/Dataset/results.csv')

    csv_match = csv.reader(match_f)

    headers = next(csv_match)
    t1 = 0
    t2 = 0

    for row in csv_match:
        if row[0] == team1:
            player1 = list(row[1].split(','))

        if row[0] == team2:
            player2 = list(row[1].split(','))

    player1 = player1[:limit]
    player2 = player2[:limit]

    print("play1 ", len(player1))
    print("play2 ", len(player2))

    data = {
        'match': match_no,
        'team1': team1,
        'team2': team2,
        'player1': player1,
        'player2': player2,
        'other_teams': other_teams,
    }
    return JsonResponse(data)


def first_inn(request):
    if request.method == 'POST':
        toss_win_team = ''
        team1 = request.POST['team1']
        team2 = request.POST['team2']
        venue = request.POST['venue']

        toss_win = request.POST['toss_win']
        decision = request.POST['decision']

        print(team1, team2, venue, toss_win, decision)
        winner_team = predict(team1, team2, venue, toss_win, decision).__str__()
        print("Winning Team is -> " + winner_team)
        home_team_name = convert_back_to_team_names(team1).__str__()
        away_team_name = convert_back_to_team_names(team2).__str__()
        response = "Match Prediction between " + home_team_name + " and " + away_team_name + " is higher for: " + winner_team
        context = {
            'message': response,
        }
        return render(request, 'fourth_umpire/firstinn.html', context)


    else:
        pass
        return render(request, 'fourth_umpire/firstinn.html', )


def prematch(request):
    team1 = ''
    x = []
    y = []
    a = []
    with open('fourth_umpire/Dataset/_team_rank.csv', 'r') as csvfile:
        next(csvfile)
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            x.append(row[0])
            if row[0] != "None":
                y.append(row[1])

    for team in x:
        if team == "Chennai Super Kings":
            a.append('CSK')

        if team == "Mumbai Indians":
            a.append('MI')

        if team == "Rajasthan Royals":
            a.append('RR')

        if team == "Kolkata Knight Riders":
            a.append('KKR')

        if team == "Delhi Daredevils":
            a.append('DD')

        if team == "Royal Challengers Bangalore":
            a.append('RCB')

        if team == "Kings XI Punjab":
            a.append('KXIP')

        if team == "Deccan Chargers":
            a.append('DC')

        if team == "Pune Warriors":
            a.append('PW')

        if team == "Gujarat Lions":
            a.append('PW')

        if team == "Kochi Tuskers Kerala":
            a.append('KTK')

        if team == "Sunrisers Hyderabad":
            a.append('SRH')

        # if team == "None":
        #    pass
    print("meth ", request.method)
    print("teams ", len(a))
    print("eigens ", len(y))

    print("teams ", a)
    print("eigens ", y)

    # x = ['MI', 'CSK', 'KKR', 'RCB', 'SRH', 'DD', 'KXIP', 'RR']
    p = figure(x_range=a, plot_height=250, title="Team rank based on eigen score")
    p.vbar(x=a, top=y, width=0.9)

    p.xgrid.grid_line_color = None
    p.y_range.start = 0
    script, div = components(p, CDN)

    if request.method == 'POST':
        year = request.POST['season']
        message = 'Eigen score :'
        if (year != '2020'):
            prediction(year)
            print("year ", year, " winner_list ", winner_list, "eigen ", Eigen_score)
            win = winner_list[1]
            prob = Eigen_score[1]
            context = {'team': team1,
                       'val': win, 'prob': prob, 'message': message, 'season': year,
                       'script': script, 'div': div,
                       }
            winner_list.clear()
            Eigen_score.clear()
            return render(request, 'fourth_umpire/pre_pred.html', context)

        else:
            with open('fourth_umpire/Dataset/test.csv', 'rt')as f:
                data = csv.reader(f)
                for row in data:
                    winner_list.append(row[1])
                    Eigen_score.append(row[6])
            win = winner_list[1]
            prob = Eigen_score[1]
            runner1 = Eigen_score[2]
            runner2 = Eigen_score[3]
            winner2 = winner_list[2]
            winner3 = winner_list[3]
            message2 = str('Runner up :1 ')
            message3 = str('Runner up :2 ')
            context = {'val': win, 'prob': prob, 'message': message, 'season': year, 'runner1': runner1,
                       'runner2': runner2, 'winner2': winner2,
                       'winner3': winner3, 'message2': message2, 'message3': message3
                       }

            winner_list.clear()
            Eigen_score.clear()
            return render(request, 'fourth_umpire/pre_pred.html', context)

            # data = pd.read_csv('results.csv')
            # data.sort_values(["Team", "sum"], axis=0,
            #                  ascending=False, inplace=True)
            # with open('results.csv', 'rt')as f:
            #     data = csv.reader(f)
            #     for row in data:
            #         winner_list.append(row[0])
            #         Eigen_score.append(row[1])
            # win = winner_list[1]
            # prob = Eigen_score[1]
            # context = {'val': win, 'prob': prob, 'message': message, 'season': year
            #            }
            # winner_list.clear()
            # Eigen_score.clear()
            # return render(request, 'fourth_umpire/pre_pred.html', context)

    else:
        pass
    return render(request, 'fourth_umpire/pre_pred.html')


def convert_back_to_team_names(team):
    team_name = ""

    if team == 'Kolkata':
        team_name = "KKR"
    if team == "Bangalore":
        team_name = "RCB"
    if team == "Pune":
        team_name = "CSK"
    if team == "Jaipur":
        team_name = "RR"
    if team == "Delhi":
        team_name = "DD"
    if team == "Dharamshala":
        team_name = "KXIP"
    if team == "Hyderabad":
        team_name = "SRH"
    if team == "Mumbai":
        team_name = "MI"

    return team_name
