import pandas as pd
from sklearn import model_selection
from sklearn.tree import DecisionTreeClassifier


def predict(home_team, away_team, city, toss_winner, toss_decision):
    matches_cleaned_data = pd.read_csv('fourth_umpire/Dataset/matches_cleaned.csv')
    matches_df = matches_cleaned_data[['team1', 'team2', 'city', 'toss_winner', 'toss_decision', 'winner']]

    # Split-out validation dataset
    array = matches_df.values
    x = array[:, 0:5]
    y = array[:, 5]
    validation_size = 0.10
    seed = 7
    x_train, x_validation, y_train, y_validation = model_selection.train_test_split(x, y, test_size=validation_size,
                                                                                    random_state=seed)

    # Test options and evaluation metric
    knn = DecisionTreeClassifier()
    knn.fit(x_train, y_train)
    results = convert_to_numerical_field(home_team, away_team, city, toss_winner, toss_decision)
    predictions = knn.predict([results])

    team = ''
    if predictions[0] == '6':
        team = 'KKR'
    if predictions[0] == "5":
        team = 'RCB'
    if predictions[0] == "9":
        team = 'CSK'
    if predictions[0] == "10":
        team = 'RR'
    if predictions[0] == "7":
        team = 'DD'
    if predictions[0] == "8":
        team = 'KXIP'
    if predictions[0] == "1":
        team = 'SRH'
    if predictions[0] == "2":
        team = 'MI'

    print("model->" + team)
    if int(predictions) != convert_again(home_team).__int__() and int(predictions) != convert_again(away_team).__int__():
            print("Exception Case")
            winner = convert_to_shortform(calculate_ef_score(home_team, away_team))
            print("EF score data ->" + winner)
            return winner
    else:
        return team.__str__()


def convert_to_shortform(winning_team):
    if winning_team == 'Kolkata':
        return 'KKR'
    if winning_team == "Bangalore":
        return 'RCB'
    if winning_team == "Pune":
        return 'CSK'
    if winning_team == "Jaipur":
        return 'RR'
    if winning_team == "Delhi":
        return 'DD'
    if winning_team == "Dharamshala":
        return 'KXIP'
    if winning_team == "Hyderabad":
        return 'SRH'
    if winning_team == "Mumbai":
        return 'MI'


def convert_again(home_team):
    if home_team == 'Kolkata':
        return 6
    if home_team == "Bangalore":
        return 5
    if home_team == "Pune":
        return 9
    if home_team == "Jaipur":
        return 10
    if home_team == "Delhi":
        return 7
    if home_team == "Dharamshala":
        return 8
    if home_team == "Hyderabad":
        return 1
    if home_team == "Mumbai":
        return 2


def convert_to_numerical_field(home_team, away_team, city, toss_winner, toss_decision):
    list = []
    print(home_team, away_team, city, toss_winner, toss_decision)
    print("list is ")
    if home_team == 'Kolkata':
        list.append(6)
    if home_team == "Bangalore":
        list.append(5)
    if home_team == "Pune":
        list.append(9)
    if home_team == "Jaipur":
        list.append(10)
    if home_team == "Delhi":
        list.append(7)
    if home_team == "Dharamshala":
        list.append(8)
    if home_team == "Hyderabad":
        list.append(1)
    if home_team == "Mumbai":
        list.append(2)

    if away_team == "Kolkata":
        list.append(6)
    if away_team == "Bangalore":
        list.append(5)
    if away_team == "Pune":
        list.append(9)
    if away_team == "Jaipur":
        list.append(10)
    if away_team == "Delhi":
        list.append(7)
    if away_team == "Dharamshala":
        list.append(8)
    if away_team == "Hyderabad":
        list.append(1)
    if away_team == "Mumbai":
        list.append(2)

    if city == "Kolkata":
        list.append(7)
    if city == "Bangalore":
        list.append(5)
    if city == "Pune":
        list.append(2)
    if city == "Jaipur":
        list.append(11)
    if city == "Delhi":
        list.append(8)
    if city == "Dharamshala":
        list.append(24)
    if city == "Hyderabad":
        list.append(1)
    if city == "Mumbai":
        list.append(6)
    if city == "Rajkot":
        list.append(3)
    if city == "Indore":
        list.append(4)
    if city == "Chandigarh":
        list.append(9)
    if city == "Kanpur":
        list.append(10)
    if city == "Chennai":
        list.append(12)
    if city == "Cape Town":
        list.append(13)
    if city == "Port Elizabeth":
        list.append(14)
    if city == "Durban":
        list.append(15)
    if city == "Centurion":
        list.append(16)
    if city == "East London":
        list.append(17)
    if city == "Johannesburg":
        list.append(18)
    if city == "Kimberley":
        list.append(19)
    if city == "Bloemfontein":
        list.append(20)
    if city == "Ahmedabad":
        list.append(21)
    if city == "Cuttack":
        list.append(22)
    if city == "Nagpur":
        list.append(23)
    if city == "Kochi":
        list.append(25)
    if city == "Visakhapatnam":
        list.append(26)
    if city == "Raipur":
        list.append(27)
    if city == "Ranchi":
        list.append(28)
    if city == "Abu Dhabi":
        list.append(29)
    if city == "Sharjah":
        list.append(30)
    if city == "Dubai":
        list.append(31)
    if city == "Mohali":
        list.append(32)


    if toss_winner == "KKR":
        list.append(6)
    if toss_winner == "RCB":
        list.append(5)
    if toss_winner == "CSK":
        list.append(9)
    if toss_winner == "RR":
        list.append(10)
    if toss_winner == "DD":
        list.append(7)
    if toss_winner == "KXIP":
        list.append(8)
    if toss_winner == "SRH":
        list.append(1)
    if toss_winner == "MI":
        list.append(2)

    if toss_decision == "Bat":
        list.append(2)
    if toss_decision == "Field":
        list.append(1)
    return list


# prediction from site scrape data
def calculate_ef_score(home, away):
    data = pd.read_csv('results.csv')
    home_score = list(data.loc[data['Team'] == home]['sum'])
    away_score = list(data.loc[data['Team'] == away]['sum'])
    if home_score > away_score:
        return home
    else:
        return away

# predict('Jaipur', 'Hyderabad', 'City: Jaipur', 'RR', 'Bat')
