# IPL Season and Match Predictor

This Django and Machine Learning powered Web app predicts and analyse IPL matches. The application works by fetching previous years match and season history
and using a ranking algorithm the system decides which team will win based on the total eigen score of each individual in the team. Each indivdual has its own strengths
and weakness and choosing the appropriate player will help in deciding who wins the match.
	In case of season prediction, the previous years performance of the different IPL teams are taken into consideration and using that knowledge. The best team
is declared as the winner.



## Local Setup
Create and activate a virtualenv:

```bash
virtualenv env
cd env
source bin/activate
```
Clone the repository on your local environment <br>

```bash
git clone https://swamys123@bitbucket.org/swamys123/ipl.git
```


Install the required dependencies <br>
```bash
pip3 install -r requirements.txt 
```

Run the localhost-server <br>
```bash 
python3 manage.py runserver
```

The web-app will be available at `127.0.0.1:8000` on your browser. 



- In this project we have use various algorithms like Naive Bayes, SVM, Random Forest  in training our various machine learning models. 
- And on different stages of training and testing, final model works and best fit with random forest algorithm. 
